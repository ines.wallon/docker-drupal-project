#!/bin/bash

# Script used to rsync sources to make patches.

# shellcheck source=scripts/script-parameters.sh
. "$(dirname "${BASH_SOURCE[0]}")"/script-parameters.sh local

echo -e "${COLOR_LIGHT_GREEN}Copy contrib modules.${COLOR_NC}"
#rsync -avzP --delete --exclude=".git" "${PROJECT_PATH}"/contrib/animated_gif "${APP_PATH}"/modules/custom/
#rsync -avzP --delete --exclude=".git" "${PROJECT_PATH}"/contrib/context_profile_role "${APP_PATH}"/modules/custom/
#rsync -avzP --delete --exclude=".git" "${PROJECT_PATH}"/contrib/devel_a11y "${APP_PATH}"/modules/contrib/
#rsync -avzP --delete --exclude=".git" "${PROJECT_PATH}"/contrib/devel_php "${APP_PATH}"/modules/contrib/
#rsync -avzP --delete --exclude=".git" "${PROJECT_PATH}"/contrib/drupal/core/* "${APP_PATH}"/core/
#rsync -avzP --delete --exclude=".git" "${PROJECT_PATH}"/contrib/drupal-l10n "${PROJECT_PATH}"/vendor/drupal-composer/
#rsync -avzP --delete --exclude=".git" "${PROJECT_PATH}"/contrib/field_formatter_range "${APP_PATH}"/modules/custom/
#rsync -avzP --delete --exclude=".git" "${PROJECT_PATH}"/contrib/file_extractor "${APP_PATH}"/modules/custom/
#rsync -avzP --delete --exclude=".git" "${PROJECT_PATH}"/contrib/image_styles_mapping "${APP_PATH}"/modules/custom/
#rsync -avzP --delete --exclude=".git" "${PROJECT_PATH}"/contrib/matomo "${APP_PATH}"/modules/custom/
#rsync -avzP --delete --exclude=".git" "${PROJECT_PATH}"/contrib/menu_per_role "${APP_PATH}"/modules/custom/
#rsync -avzP --delete --exclude=".git" "${PROJECT_PATH}"/contrib/renderviz "${APP_PATH}"/modules/contrib/
#rsync -avzP --delete --exclude=".git" "${PROJECT_PATH}"/contrib/speedboxes "${APP_PATH}"/modules/contrib/
#rsync -avzP --delete --exclude=".git" "${PROJECT_PATH}"/contrib/views_merge_rows "${APP_PATH}"/modules/custom/

echo -e "${COLOR_LIGHT_GREEN}Rebuild Drupal paranoia.${COLOR_NC}"
composer drupal:paranoia --working-dir="${PROJECT_PATH}"
