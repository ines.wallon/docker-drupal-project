#!/bin/bash

# shellcheck source=scripts/script-parameters.sh
. "$(dirname "$(dirname "$(dirname "${BASH_SOURCE[0]}")")")"/script-parameters.sh local

shopt -s globstar nullglob

ERROR=''

# shellcheck disable=SC2068
for YAML_FILE_PATH in ${YAML_LINT_PATHS[@]}
do
  # Ensure what is detected is a file.
  if [ -f "${YAML_FILE_PATH}" ]; then
    if ! "${PROJECT_PATH}"/vendor/bin/yaml-cli lint "${YAML_FILE_PATH}"; then
      ERROR='yes'
    fi
  fi
done

if [ "${ERROR}" = "yes" ]; then
  exit 1
fi
