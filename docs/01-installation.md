# Installation

* Execute: `make project-init-dev`
* Adapt the following files to your configuration:
  * .env
  * conf/env/composer.env
  * conf/drupal/default/settings.local.php
    * uncomment lines in the "Development" section.
  * conf/drupal/sites.php
* Execute:
```bash
make docker-up
make docker-site-install SELECTED_SITE=all
```

## General note

Inside the `web` container you can launch scripts directly if you want, but it
is preferred to use the `Makefile` to execute scripts.
