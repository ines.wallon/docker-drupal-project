# Requirements

## Tools

Tools required to use this project:
* Bash >= 4
* Make
* Docker
* Docker compose

## Deployment

When deploying, on the target servers, the user used to connect to the server
should be able to launch Drush commands as the webserver user. Example:

```bash
sudo -u www-data drush
```

And be able to create/remove/change files and symlinks in the folder that matches
the `DEPLOYMENT_PATH` variable value.

### Docker environment

The user used to connect to the server should be added to the Docker group to be
able to launch docker commands.
