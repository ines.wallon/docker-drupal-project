# Development

## Development modules

To get development modules, in the .env file, have `ENVIRONMENT_MODE="dev"`.
Or just run the command `composer install`.

## Xdebug

To have Xdebug enabled in the web container, in the file .env, set the variable
`XDEBUG_ENABLED=true`.

## Debug mode

If you want to be in debug mode, uncomment lines from
`conf/drupal/default/settings.local.php` in the "Development" section.
