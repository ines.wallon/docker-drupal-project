# Docker

## Overview

```mermaid
flowchart TD
  subgraph Network[Project Docker network]
    direction TB

    web[Apache + PHP - :80]
    varnish[Varnish - :80]
    mail[Maildev - :25 : 80]

    subgraph Dev[Dev tools]
        direction LR
        node[NodeJS]
        chrome[Chromedriver - :9515]
        pa11y[Pa11y]
        cypress[Cypress]
    end

    subgraph Backends
        direction LR
        mysql[MariaDB - :3306]
        redis[Redis - :6379]
        solr[Solr - :8983]
        cron[Cron - Apache + PHP - :80]
    end

  end

  Request --> traefik
  traefik[Traefik :80 :443] -- SSL Redirect--> traefik
  traefik -- https://web-ddp9.docker.localhost --> web
  traefik -- https://varnish-ddp9.docker.localhost --> varnish
  traefik -- https://mail-ddp9.docker.localhost --> mail
  cron --> web
  varnish --> web
  web --> mysql
  web --> redis
  web --> solr
  web <-- for tests --> chrome
  pa11y -- for tests --> web
  cypress -- for tests --> web
```

## Traefik integration

If there's a local traefik reverse-proxy on your development environment,
you will have:
* the website accessible through Apache:
  * https://web-ddp9.docker.localhost
  * https://en-web-ddp9.docker.localhost
  * https://fr-web-ddp9.docker.localhost
* the website accessible through Varnish:
  * https://varnish-ddp9.docker.localhost
  * https://en-varnish-ddp9.docker.localhost
  * https://fr-varnish-ddp9.docker.localhost
* a mail catcher: https://mail-ddp9.docker.localhost

Alternative hostnames can be provided by setting environment variables, for
instance using a .env file. See the example.env file for the available
variables.

A Traefik configuration and docker-compose.yml can be found on [this repository](https://gitlab.com/florenttorregrosa-docker/apps/docker-traefik)
to ease the usage.

Note: You may have to adapt your DNS configuration to inform your computer to
search for local websites. This step is optional if you use the domain
`docker.localhost`.

For example, you can edit your /etc/hosts file and add an entry like:
```
127.0.0.1  web-ddp9.docker.localhost en-web-ddp9.docker.localhost fr-web-ddp9.docker.localhost varnish-ddp9.docker.localhost en-varnish-ddp9.docker.localhost fr-varnish-ddp9.docker.localhost mail-ddp9.docker.localhost
```

## By port mapping

Otherwise it depends if you have made a port mapping in the docker-compose.yml
file, for example:

```yaml
services:
  web:
    ...
    ports:
      - 8081:80
```

The website **should** be located at this address: `http://127.0.0.1:8081/*`

## Q/A
### How to use Drush within Docker?

Using Makefile:

```bash
make docker-drush cr
```

Or when in the container, you have to "be" in the app folder (eg: `/project/app`) or the project
folder (eg: `/project`)

Optional: you can use Drush within the web container by using the alias
`@default.alias`:

```bash
drush @default.alias status
```

### How to import a custom dump?

Inside the `web` container:

```bash
cd /project/backups
tar -xvzf /project/backups/DUMP_NAME.sql.gz
cd ..
drush sql:cli < /project/backups/DUMP_NAME.sql
```
